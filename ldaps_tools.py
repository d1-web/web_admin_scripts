import subprocess
import os

#
# Various options
#

department = "D1"
database_keys = ['givenName','sn','cn','uid', 'istEmployeeAffiliation', 'istIsFormer', 'istIsDisabled', 'istIsMemberOf']
#role resolvement orders for current and former members
current_role_resolvement_order = ['guest', 'director', 'secretary', 'researcher', 'student']
former_role_resolvement_order = ['old_researcher', 'old_guest', 'old_student']

# maps from role to keywords in the affiliation string
role_dict = {
		'guest' : ['Guest'],
		'director' : ['Director'],
		'researcher' : ['Research Leader', 'Junior Research Leader', 'Senior Researcher', 'Researcher', 'Post Doc', 'PhD Student', 'Staff Member'],
		'student' : ['Student', 'HiWi', 'Masters', 'Bachelor', 'Internship'],
		'secretary' : ['Secretary'],
		'old_guest' : ['Guest'],
		'old_director' : ['Director'],
		'old_researcher' : ['Research Leader', 'Junior Research Leader', 'Senior Researcher', 'Researcher', 'Post Doc', 'PhD Student', 'Staff Member'],
		'old_student' : ['Student', 'HiWi', 'Masters', 'Bachelor', 'Internship'],
		'old_secretary' : ['Secretary']
		}

#
# Other stuff
#

class Person(object):
	"""Stores one person. Database keys are transformed to properties of the object, e.g. person.givenName produces the given name, if it is collected from ldaps"""
	def __init__(self, ldaps_string):
		self._parse(ldaps_string)
		self.ldaps_string = ldaps_string

	def get_affiliation(self):
		"""Since people might have multiple affiliations, this function can be used to get one of the highest priority. If the 'PRIMARY' affiliation is in D1 that one is returned. Otherwise all 'NORMAL' aff. in D1 are returned."""
		#Case 1. only one affiliation, it must be correct department
		if not isinstance(self.istEmployeeAffiliation, list):
			assert('D1' in self.istEmployeeAffiliation)
			return self.istEmployeeAffiliation.split(';')
		fields = []
		#Case 2. Try to return primary affiliation
		for entry in self.istEmployeeAffiliation:
			if 'PRIMARY' in entry and 'D1' in entry:
				fields.extend(entry.split(';'))
		if fields: return fields
		#Case 3. No primary affiliation in D1
		for entry in self.istEmployeeAffiliation:
			if 'NORMAL' in entry and 'D1' in entry:
				fields.extend(entry.split(';'))
		if fields: return fields
		raise ValueError("can't parse affiliation " + str(self))

	def get_role(self):
		"""Return a role as defined in the global variables. Return 'other' if nothing matches"""
		aff = self.get_affiliation()
		if not self.is_former():
			for key in current_role_resolvement_order:
				if any((keyword in aff for keyword in role_dict[key])):
					return key
			return 'other'
		else:
			for key in former_role_resolvement_order:
				if any((keyword in aff for keyword in role_dict[key])):
					return key
			return 'old_other'

	def is_former(self):
		return (self.istIsFormer=='TRUE' or self.istIsDisabled == 'TRUE')

	def is_disabled(self):
		return self.istIsDisabled=='TRUE'

	def _parse(self, input_string):
		this_dict = self.__dict__
		entries = input_string.split(':')
		for entry in entries:
			if entry == '': continue
			key, value = entry.split('=')
			if key in this_dict:
				l = [value]
				if isinstance(this_dict[key], list):
					l.extend(this_dict[key])
				else:
					l.append(this_dict[key])
				this_dict[key] = l
			else:
				this_dict[key] = value

	def __str__(self):
		return str(self.uid)

	def __repr__(self):
		return str(self.__dict__)

def get_people():
	""" Ask ldaps for everybody in the department as  configured in options"""
	p = subprocess.Popen(['ldaps','-l','&(istEmployeeAffiliation=*'+department+'*'+')(!(istIsPseudoAccount=TRUE))'] + database_keys, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()
	if p.returncode != 0:
		raise IOError("ldaps returned a non-zero return code")
	out = out.decode('latin-1')
	return map(Person,out.split('\n')[:-1])

def get_person(uid):
	"""Fetch the uid from ldaps"""
	p = subprocess.Popen(['ldaps', '-l', '&(uid='+uid+')']+database_keys, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()
	if p.returncode != 0:
		raise IOError("ldaps returned a non-zero return code")
	out = out.decode('latin-1')
	return Person(out[:-1])

def get_website_file(person):
	""" Get the path to this persons personal homepage """
	uid = person.uid
	website_prefix = "/www/inf-homepage/"
	assert os.path.isdir(website_prefix)
	website_path = website_prefix+uid+"/index.html"
	return website_path

def get_website_url(person):
	""" get a persons website. note: does not work for people with mmci affiliation """
	uid = person.uid
	website_prefix = "http://people.mpi-inf.mpg.de/"
	return website_prefix+"~"+uid+"/" #the slash at the end is important


def get_role_dict(people):
	""" Create a dict role -> [people] for these people """
	role_people = dict((k, list()) for k in role_dict.keys())
	for p in people:
		role = p.get_role()
		role_list = role_people.get(role,list())
		role_list.append(p)
		role_people[role] = role_list
	return role_people

def get_current_people():
	""" Ask ldaps for everybody who is neither a former member nor has an disabled account """
	return [p for p in get_people() if not (p.is_former() or p.is_disabled())]

def get_current_staff():
	""" Ask ldaps for everybody who is staff, i.e. no students, no guests, ect ... """
	return [ p for p in get_current_people() if (p.get_role() == "researcher" or p.get_role() == "secretary") ]

if __name__=='__main__':
	import sys
	print(sys.argv[1])
	print(repr(get_person(sys.argv[1])))
