#!/usr/bin/env python3
import subprocess
import locale
import ldaps_tools

locale.setlocale(locale.LC_ALL, 'en_US')
__author__ = ['Adrian Neumann aneumann@mpi-inf.mpg.de']

from email.mime.text import MIMEText
import smtplib

web_admin_mail = 'd1-web-adm-mails'
storage_file = '/home/d1-web/web_admin_scripts/people_already_reminded.txt'

def everybody():
	"""returns a list of all usernames in the department"""
	return [p.uid for p in ldaps_tools.get_current_people()]

def staff():
	"""returns a list of all usernames considered department staff"""
	return [p.uid for p in ldaps_tools.get_current_staff()]

def already_reminded_people():
	"""returns a list of usernames that have already been reminded"""
	with open(storage_file,'r') as f:
		return f.read().split('\n')

def remind(uid):
	"""send an email to uid, remember that uid was reminded"""
	msg_string = "Hello,\n\nthis is a script that reminds people who just got"
	msg_string +=" a new MPI account to check the research areas here"
	msg_string +="\n\n>https://www.mpi-inf.mpg.de/departments/algorithms-complexity/research/\n\n"
	msg_string +="and send an email to " + web_admin_mail + "@mpi-inf.mpg.de" +" to tell our webadmin where you belong. "
	msg_string +="\n\nGreets,\n\nthe administrative robots"
	msg = MIMEText(msg_string)
	msg['Subject'] = "Please contact our web admin"
	msg['From'] = web_admin_mail+'@mpi-inf.mpg.de'
	msg['To'] = uid+"@mpi-inf.mpg.de"
	s = smtplib.SMTP('localhost')
	s.sendmail(web_admin_mail+"@mpi-inf.mpg.de", [web_admin_mail+"@mpi-inf.mpg.de", uid+"@mpi-inf.mpg.de"], msg.as_string())
	with open(storage_file, 'a') as f:
		f.write(uid+'\n')

def remind_new_people():
	""" remind all accounts that have not yet been reminded """
	all = staff()
	reminded = set(already_reminded_people())
	for uid in all:
		if uid in reminded: continue
		remind(uid)

remind_new_people()
